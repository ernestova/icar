//
//  IcarSDK.h
//  IcarSDK
//
//  Created by Enric Vergara on 5/1/17.
//  Copyright © 2017 icarvision. All rights reserved.
//

#import <UIKit/UIKit.h>


//! Project version number for IcarSDK.
FOUNDATION_EXPORT double IcarSDKVersionNumber;

//! Project version string for IcarSDK.
FOUNDATION_EXPORT const unsigned char IcarSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IcarSDK/PublicHeader.h>
#import <IcarSDK/IcarSDK_Defs.h>
#import <IcarSDK/IcarCapture_Configuration.h>
#import <IcarSDK/IcarCapture_NC.h>
#import <IcarSDK/IcarCapture_VC.h>
#import <IcarSDK/IdCloud_DataResult.h>
#import <IcarSDK/IdCloud_LocalImages.h>
#import <IcarSDK/IdCloud_Result.h>
#import <IcarSDK/IdCloud_Connection.h>
#import <IcarSDK/IdCloud_Configuration.h>
#import <IcarSDK/IcarPayment_Result.h>
#import <IcarSDK/IcarPayment_Manager.h>
#import <IcarSDK/IcarPayment_Configuration.h>
#import <IcarSDK/IcarParsedPdf417.h>
#import <IcarSDK/IcarParsedMrz.h>












