//
//  DemoSDK_Constants.h
//  Demo SDK
//
//  Created by Enric Vergara on 29/6/16.
//  Copyright © 2016 IcarVision. All rights reserved.
//

#ifndef DemoSDK_Constants_h
#define DemoSDK_Constants_h


//------ICAR LICENSE KEY-----------
//BundleID: com.icarvision.CaptureAndSend
//Expiry Date: Febrary 25, 2017
//License:
static NSString *const DemoSDK_icarLicenseKey = @"k13aHFSjAidpHsfNvY/7JI6y5QRjibdWOhok3L81hibdVecICee0sQOEf55s/N0lYK0/lz4GTDq9FHqyhtKnGWpoI4BVszTH7lWkUm9uSniMlfTsvc/9WDlMS1jx21R9N8HHUJwMrLOLKylF1ooQbDErsDRqfGZx4Iz9YL1R3O2o1zMaKUxQ/Zoqxh89s0YTJR18eXd67bpxTg6u1ZLTRw==";
//---------------------------------


//----CONNECTION DATA INFO---------
static NSString *const DemoSDK_IDCloud_User     = @"IDCloud_User";
static NSString *const DemoSDK_IDCloud_Password = @"IDCloud_Pwd";
static NSString *const DemoSDK_IDCloud_Company  = @"IDCloud_Company";
//---------------------------------



#endif /* DemoSDK_Constants_h */
