//
//  AppDelegate.h
//  OfflineProcesses
//
//  Created by Enric Vergara on 30/3/17.
//  Copyright © 2017 icarvision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

