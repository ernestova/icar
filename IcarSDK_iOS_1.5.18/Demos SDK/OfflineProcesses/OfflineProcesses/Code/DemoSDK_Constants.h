//
//  DemoSDK_Constants.h
//  Demo SDK
//
//  Created by Enric Vergara on 29/6/16.
//  Copyright © 2016 IcarVision. All rights reserved.
//

#ifndef DemoSDK_Constants_h
#define DemoSDK_Constants_h


//------ICAR LICENSE KEY-----------
//BundleID: com.icarvision.OfflineProcesses
//Expiry Date: Febrary 25, 2017
//License:
static NSString *const icarLicenseKey = @"hqL9PFVaSFgFpq3h4HWjnnPbZNvvY/089RBVvwJlW00zXadspJzSE9Ub7etCmz6laT8A4cekJCk2XSH1IblS/a/aGbnmshS+z7sUFUuq//qpL5D6mFqPztDllcnlJOgwV5I3xOmeNaVK3QrnBwjcY2AG0WJNKt0aH8oNXRL6rZI6mfwHacY00MAsnvacZIANaLDy+LyKecewke4wxIRvvw==";
//---------------------------------


//----CONNECTION DATA INFO---------
static NSString *const DemoSDK_Payment_User     = @"Payment_User";
static NSString *const DemoSDK_Payment_Password = @"Payment_Pwd";
static NSString *const DemoSDK_Payment_Company  = @"Payment_Company";
//---------------------------------



#endif /* DemoSDK_Constants_h */
